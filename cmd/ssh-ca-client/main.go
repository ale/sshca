package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"git.autistici.org/ale/sshca"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

var (
	rootServerAddr = flag.String("rootca-addr", "localhost:3993", "address (host:port) of the root CA service")
	caPath         = flag.String("ca", "/var/lib/ssh-ca/root_ca", "root CA certificate private key")
	isUser         = flag.Bool("user", false, "sign a user certificate")
	isHost         = flag.Bool("host", false, "sign a host certificate (default)")
)

func doRequest(ha *client.Client, req *http.Request) (*http.Response, error) {
	tlsConf, err := ha.ClientTlsConfig(req.URL.Host)
	if err != nil {
		return nil, err
	}
	tr := &http.Transport{
		TLSClientConfig:    tlsConf,
		DisableCompression: true,
	}
	httpClient := &http.Client{Transport: tr}
	return httpClient.Do(req)
}

func doJsonRequest(ha *client.Client, method, uri string, req, resp interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return err
	}
	httpReq, err := http.NewRequest(method, uri, &buf)
	if err != nil {
		return err
	}
	httpReq.Header.Set("Content-Type", "application/json")

	httpResp, err := doRequest(ha, httpReq)
	if err != nil {
		return err
	}
	defer httpResp.Body.Close()
	if httpResp.StatusCode != 200 {
		return fmt.Errorf("HTTP error: %v", httpResp.Status)
	}
	if httpResp.Header.Get("Content-Type") != "application/json" {
		return fmt.Errorf("bad Content-Type %s", httpResp.Header.Get("Content-Type"))
	}
	return json.NewDecoder(httpResp.Body).Decode(resp)
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	if flag.NArg() != 1 {
		log.Fatal("Wrong number of arguments. Usage: ssh-ca-client <.pub file>")
	}
	inputfile := flag.Arg(0)
	outputfile := strings.Replace(inputfile, ".pub", "-cert.pub", 1)
	if inputfile == outputfile {
		outputfile = inputfile + "-cert"
	}

	if *isUser && *isHost {
		log.Fatal("Specify only one of --user or --host")
	}
	if !*isUser {
		*isHost = true
	}

	publicKey, err := ioutil.ReadFile(inputfile)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}

	ha, err := client.New()
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}
	defer ha.Close()

	req := &sshca.SignKeyRequest{
		Name:      util.Hostname,
		PublicKey: publicKey,
		IsHost:    *isHost,
	}
	log.Printf("Request: %#v", req)
	var resp sshca.SignKeyResponse
	err = doJsonRequest(ha, "POST", fmt.Sprintf("https://%s/sign", *rootServerAddr), req, &resp)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}

	f, err := os.Create(outputfile)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}
	defer f.Close()
	f.Write(resp.Certificate)

	fmt.Printf("new key saved to %s\n", outputfile)
}
