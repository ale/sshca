package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"

	"git.autistici.org/ale/sshca"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util/nssutil"
)

var (
	addr    = flag.String("addr", ":3993", "tcp port to listen on")
	aclSpec = flag.String("acl", "root", "ACL for client authorization (default: allow only the root user)")
	caPath  = flag.String("ca", "/var/lib/ssh-ca/root_ca", "root CA certificate private key")
	keyType = flag.String("type", "ed25519", "CA key type")
)

type aclWrapper struct {
	acl    hostauth.ACL
	aclCtx hostauth.ACLContext
	h      http.Handler
}

func newAclWrapper(h http.Handler) *aclWrapper {
	// Initialize the hostauth subsystem using command-line flags.
	acl, err := hostauth.ParseACL(*aclSpec)
	if err != nil {
		log.Fatalf("error parsing ACL: %v", err)
	}
	aclCtx := nssutil.NewNSSAclContext()
	return &aclWrapper{
		acl:    acl,
		aclCtx: aclCtx,
		h:      h,
	}
}

func (s *aclWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	creds, err := client.CheckPeerCredentials(r.TLS, s.acl, s.aclCtx)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		log.Printf("unauthorized request from %s", creds)
		return
	}

	s.h.ServeHTTP(w, r)
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	// Initialize the CA.
	ca, err := sshca.NewCA(*caPath, *keyType)
	if err != nil {
		log.Printf("could not initialize CA: %v", err)
	}

	r := http.NewServeMux()
	r.HandleFunc("/sign", func(w http.ResponseWriter, r *http.Request) {
		// A few sanity checks.
		if r.Method != "POST" {
			http.Error(w, "Bad Request", http.StatusMethodNotAllowed)
			return
		}
		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Bad Content-Type", http.StatusBadRequest)
			return
		}

		// Call Sign on the deserialized JSON request, and then encode the response.
		var req sshca.SignKeyRequest
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			log.Printf("http: json decode error: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		resp, err := ca.Sign(&req)
		if err != nil {
			log.Printf("http: sign error: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(resp)
	})
	srv := newAclWrapper(r)

	// Set up hostauth and use it to create a SSL listener.
	c, err := client.New()
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	l, err := client.Listen(*addr, c)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("starting https server on %s", *addr)
	if err := http.Serve(l, srv); err != nil {
		log.Fatalf("http listen error: %v", err)
	}
}
