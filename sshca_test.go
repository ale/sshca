package sshca

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestSign(t *testing.T) {
	dir, _ := ioutil.TempDir("", "test-")
	defer os.RemoveAll(dir)

	// Create a test host key.
	if err := runKeygen("-q", "-N", "", "-t", "ed25519", "-f", filepath.Join(dir, "host_key")); err != nil {
		t.Fatal("generating host key: %v", err)
	}
	pubFile := filepath.Join(dir, "host_key.pub")
	pubData, _ := ioutil.ReadFile(pubFile)

	ca, err := NewCA(filepath.Join(dir, "ca"), "ed25519")
	if err != nil {
		t.Fatalf("NewCA(): %v", err)
	}
	resp, err := ca.Sign(&SignKeyRequest{
		Name:      "testhost",
		PublicKey: pubData,
		IsHost:    true,
	})
	if err != nil {
		t.Fatalf("Sign(): %v", err)
	}
	if len(resp.Certificate) == 0 {
		t.Fatal("Sign returned empty cert")
	}
}
