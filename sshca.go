package sshca

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type SignKeyRequest struct {
	Name      string `json:"name"`
	IsHost    bool   `json:"is_host"`
	PublicKey []byte `json:"host_public_key"`
}

type SignKeyResponse struct {
	Certificate []byte `json:"host_cert"`
	RootPublicKey []byte `json:"root_public_key"`
}

type tempContext struct {
	dir string
}

func newTempContext() *tempContext {
	dir, err := ioutil.TempDir("", "ssh-keygen-")
	if err != nil {
		panic(err)
	}
	return &tempContext{dir: dir}
}

func (c *tempContext) TempFile(name string, contents []byte) string {
	path := filepath.Join(c.dir, name)
	if err := ioutil.WriteFile(path, contents, 0600); err != nil {
		panic(err)
	}
	return path
}

func (c *tempContext) ReadFile(name string) ([]byte, error) {
	return ioutil.ReadFile(filepath.Join(c.dir, name))
}

func (c *tempContext) Close() {
	os.RemoveAll(c.dir)
}

type CA struct {
	PublicKey []byte
	PrivateKeyPath string
	ValidDays int
}

func runKeygen(args ...string) error {
	cmd := exec.Command("/usr/bin/ssh-keygen", args...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stderr
	log.Printf("exec ssh-keygen %s", strings.Join(args, " "))
	return cmd.Run()
}

func NewCA(privkey string, keyType string) (*CA, error) {
	// Generate all root certificates that might be missing.
	if _, err := os.Stat(privkey); err != nil {
		if err := generateRootKey(privkey, keyType); err != nil {
			return nil, err
		}
	}
	// Read the public key data.
	pubkey := privkey + ".pub"
	pkdata, err := ioutil.ReadFile(pubkey)
	if err != nil {
		return nil, err
	}
	return &CA{
		PublicKey: pkdata,
		PrivateKeyPath: privkey,
		ValidDays: 365,
	}, nil
}

func generateRootKey(keyFile, keyType string) error {
	return runKeygen("-q", "-N", "", "-t", keyType, "-C", "Root CA", "-f", keyFile)
}

var privateKeyTagBytes = []byte("-----BEGIN OPENSSH PRIVATE KEY-----")

// Sign a host key.
func (ca *CA) Sign(req *SignKeyRequest) (*SignKeyResponse, error) {
	// Sanity check: the host key must not be null.
	if len(req.PublicKey) == 0 {
		return nil, errors.New("host public key is nil")
	}
	if req.Name == "" {
		return nil, errors.New("name is nil")
	}
	// Or, this may be a private key (!)
	if bytes.HasPrefix(req.PublicKey, privateKeyTagBytes) {
		return nil, errors.New("the key looks like a private key")
	}

	// Operate within a temporary directory.
	tc := newTempContext()
	defer tc.Close()

	// Build the ssh-keygen command. The only logic is that we add
	// a "-h" option if IsHost is true in the incoming request.
	signCmd := []string{"-s", ca.PrivateKeyPath, "-n", req.Name, "-V", fmt.Sprintf("+%dd", ca.ValidDays), "-I", req.Name}
	if req.IsHost {
		signCmd = append(signCmd, "-h")
	}
	signCmd = append(signCmd, tc.TempFile("host_key.pub", req.PublicKey))
	if err := runKeygen(signCmd...); err != nil {
		return nil, err
	}
	cert, err := tc.ReadFile("host_key-cert.pub")
	if err != nil {
		return nil, err
	}
	return &SignKeyResponse{
		Certificate: cert,
	}, nil
}

