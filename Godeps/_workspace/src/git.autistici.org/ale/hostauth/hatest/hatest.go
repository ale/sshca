package hatest

import (
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	local_proto "git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local"
	local_server "git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local/server"
	root_server "git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/root/server"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

// A HostauthContext creates a new standalone PKI, runs a root server
// and a local server, and can provide callers with a Client that is
// setup to talk to them.
type HostauthContext struct {
	tmpdir     string
	encPw      []byte
	rootca     *hostauth.CA
	roots      *x509.CertPool
	rootSrv    *root_server.Server
	rootHttp   *httptest.Server
	hostca     *hostauth.CA
	hostSrv    *local_server.Server
	hostHttp   *httptest.Server
	hostSocket string
}

func CreateContext() (*HostauthContext, error) {
	tmpdir, err := ioutil.TempDir("", "")
	if err != nil {
		return nil, err
	}

	encryptionPw := []byte("secret")

	// Create root server.
	rootca, err := hostauth.CreateRootCA()
	if err != nil {
		return nil, err
	}
	pool := x509.NewCertPool()
	pool.AddCert(rootca.Certificate)
	//rootKeyData, _ := hostauth.MarshalPrivateKey(rootca.PrivateKey)
	rootSrv, err := root_server.NewServer(rootca, encryptionPw)
	if err != nil {
		return nil, err
	}
	rootHttp := httptest.NewUnstartedServer(rootSrv)
	rootHttp.TLS, _ = rootSrv.TlsConfig()
	rootHttp.StartTLS()
	u, _ := url.Parse(rootHttp.URL)
	rootCaAddr := u.Host

	// Create host server.
	csr, priv, err := hostauth.CreateCertificateRequest(hostauth.HostCAName(util.Hostname), nil, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create CSR for CA: %v", err)
	}
	crt, err := rootca.CreateCA(csr)
	if err != nil {
		return nil, fmt.Errorf("could not sign CSR for CA: %v", err)
	}
	keyData, _ := hostauth.MarshalPrivateKey(priv)
	localCA, err := hostauth.NewCAFromBytes(keyData, crt.Raw)
	hostSrv, err := local_server.NewServer(localCA, pool, rootCaAddr)
	if err != nil {
		return nil, err
	}
	hostHttp := httptest.NewServer(hostSrv)

	socketPath := filepath.Join(tmpdir, "local-socket")
	unixSrv := util.NewUnixServer(hostSrv)
	go unixSrv.ListenAndServe(socketPath)

	return &HostauthContext{
		hostSocket: socketPath,
		tmpdir:     tmpdir,
		encPw:      encryptionPw,
		rootca:     rootca,
		roots:      pool,
		rootSrv:    rootSrv,
		rootHttp:   rootHttp,
		hostca:     localCA,
		hostSrv:    hostSrv,
		hostHttp:   hostHttp,
	}, nil
}

func (c *HostauthContext) NewClient() (*client.Client, error) {
	hostPort := getPort(c.hostHttp.URL)
	return client.NewClient(
		local_proto.NewSocketSignerProtocol(c.hostSocket),
		local_proto.NewHostCertGetter(hostPort, c.roots),
		c.roots,
	)
}

func (c *HostauthContext) Close() {
	c.rootHttp.Close()
	c.hostHttp.Close()
	os.RemoveAll(c.tmpdir)
}

func getPort(uri string) int {
	u, _ := url.Parse(uri)
	_, hostPortStr, err := net.SplitHostPort(u.Host)
	if err != nil {
		return 80
	}
	p, _ := strconv.Atoi(hostPortStr)
	return p
}
