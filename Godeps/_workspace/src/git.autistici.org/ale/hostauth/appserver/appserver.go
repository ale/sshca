package appserver

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/appserver/fcgi"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
)

type AppServerOptions struct {
	RunDir             string
	NumWorkers         int
	ACL                hostauth.ACL
	ACLContext         hostauth.ACLContext
	EnableMultiplexing bool
}

type AppServer struct {
	workers []*worker
	stop    chan bool
	wg      sync.WaitGroup
	//pool    *connectionPool
	cache *connectionCache

	acl    hostauth.ACL
	aclCtx hostauth.ACLContext

	lock  sync.Mutex
	rrPtr int
}

type worker struct {
	cmd        []string
	socketPath string
	proc       *exec.Cmd
}

func (w *worker) runner(stop <-chan bool) {
	log.Printf("starting worker on socket %s", w.socketPath)
	defer os.Remove(w.socketPath)

	childCh := make(chan error, 1)

	runCmd := func() {
		cmd := exec.Command(w.cmd[0], w.cmd[1:]...)
		w.proc = cmd
		childCh <- cmd.Run()
	}

	go runCmd()

	exiting := false
	for {
		select {
		case err := <-childCh:
			log.Printf("child process terminated: %v", err)
			if exiting {
				// We're done here.
				return
			}
			// Restart.
			time.Sleep(3 * time.Second)
			go runCmd()
		case <-stop:
			if w.proc != nil {
				w.proc.Process.Kill()
			}
			exiting = true
		}
	}
}

// type connectionFunc func() (*fcgiclient.Conn, error)

// type connectionPool struct {
// 	size   int
// 	connFn connectionFunc
// 	conn   chan *fcgiclient.Conn
// }

// func newConnectionPool(size int, fn connectionFunc) *connectionPool {
// 	return &connectionPool{
// 		size:   size,
// 		connFn: fn,
// 		conn:   make(chan *fcgiclient.Conn, size),
// 	}
// }

// func (p *connectionPool) GetConnection() (*fcgiclient.Conn, error) {
// 	select {
// 	case conn := <-p.conn:
// 		return conn, nil
// 	default:
// 		return p.connFn()
// 	}
// }

// func (p *connectionPool) ReleaseConnection(conn *fcgiclient.Conn) {
// 	select {
// 	case p.conn <- conn:
// 	default:
// 		conn.Close()
// 	}
// }

type connectionCache struct {
	sync.Mutex
	cache map[string]*fcgiclient.Conn
}

func newConnectionCache() *connectionCache {
	return &connectionCache{
		cache: make(map[string]*fcgiclient.Conn),
	}
}

func (c *connectionCache) Get(addr string) (*fcgiclient.Conn, error) {
	c.Lock()
	defer c.Unlock()
	conn, ok := c.cache[addr]
	if !ok {
		var err error
		conn, err = fcgiclient.Dial("unix", addr)
		if err != nil {
			return nil, err
		}
		c.cache[addr] = conn
	}
	return conn, nil
}

func (c *connectionCache) Invalidate(addr string, conn *fcgiclient.Conn) {
	c.Lock()
	defer c.Unlock()
	if oldConn, ok := c.cache[addr]; ok && oldConn == conn {
		conn.Close()
		delete(c.cache, addr)
	}
}

func expandVars(cmd []string, socketPath string) []string {
	var out []string
	for _, s := range cmd {
		s = strings.Replace(s, "%s", socketPath, -1)
		out = append(out, s)
	}
	return out
}

func NewFastCGIAppServer(cmd []string, opts *AppServerOptions) *AppServer {
	var defaultOpts AppServerOptions
	if opts == nil {
		opts = &defaultOpts
	}

	s := &AppServer{
		acl:    opts.ACL,
		aclCtx: opts.ACLContext,
		stop:   make(chan bool),
	}

	numWorkers := opts.NumWorkers
	if numWorkers <= 0 {
		numWorkers = 1
	}
	for i := 0; i < numWorkers; i++ {
		sp := filepath.Join(opts.RunDir, fmt.Sprintf("socket-%d-%d", os.Getpid(), rand.Int63()))
		w := &worker{
			socketPath: sp,
			cmd:        expandVars(cmd, sp),
		}
		s.workers = append(s.workers, w)

		s.wg.Add(1)
		go func(w *worker) {
			w.runner(s.stop)
			s.wg.Done()
		}(w)
	}

	//s.pool = newConnectionPool(workers*5, s.Dial)
	if opts.EnableMultiplexing {
		s.cache = newConnectionCache()
	}

	return s
}

func (s *AppServer) Close() error {
	close(s.stop)
	s.wg.Wait()
	return nil
}

// Endpoint returns a new UNIX socket path on every call, rotating
// among available workers with a simple round-robin algorithm.
func (s *AppServer) Endpoint() string {
	s.lock.Lock()
	endpoint := s.workers[s.rrPtr].socketPath
	s.rrPtr++
	if s.rrPtr >= len(s.workers) {
		s.rrPtr = 0
	}
	s.lock.Unlock()
	return endpoint
}

func (s *AppServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var creds hostauth.Credentials
	var err error
	if s.acl != nil {
		// Check the remote peer credentials against our ACL.
		creds, err = client.CheckPeerCredentials(req.TLS, s.acl, s.aclCtx)
		if err != nil {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}
	}

	var fcgi *fcgiclient.Conn
	addr := s.Endpoint()
	if s.cache != nil {
		fcgi, err = s.cache.Get(addr)
	} else {
		fcgi, err = fcgiclient.Dial("unix", addr)
		if err == nil {
			defer fcgi.Close()
		}
	}
	if err != nil {
		http.Error(w, "Unavailable", http.StatusServiceUnavailable)
		log.Printf("503: %v", err)
		return
	}
	//defer s.pool.ReleaseConnection(fcgi)

	host, port, err := net.SplitHostPort(req.URL.Host)
	if err != nil {
		port = "80"
	}

	env := map[string]string{
		"REQUEST_METHOD":  req.Method,
		"REMOTE_ADDR":     req.RemoteAddr,
		"SERVER_NAME":     host,
		"SERVER_PORT":     port,
		"SERVER_PROTOCOL": "https",
		"CONTENT_TYPE":    req.Header.Get("Content-Type"),
		"CONTENT_LENGTH":  req.Header.Get("Content-Length"),
		"REQUEST_URI":     req.URL.RequestURI(),
		"SCRIPT_NAME":     "/",
	}
	if s.acl != nil {
		env["HOSTAUTH_CREDENTIALS"] = creds.String()
	}
	for hdr, value := range req.Header {
		envName := fmt.Sprintf("HTTP_%s", strings.Replace(strings.ToUpper(hdr), "-", "_", -1))
		env[envName] = value[0]
	}

	resp, err := fcgi.HttpRequest(env, req.Body)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("500: %v", err)
		if s.cache != nil {
			s.cache.Invalidate(addr, fcgi)
		}
		return
	}
	defer resp.Body.Close()

	for hdr, values := range resp.Header {
		for _, v := range values {
			w.Header().Add(hdr, v)
		}
	}
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}

// func main() {
// 	flag.Parse()

// 	runtime.GOMAXPROCS(runtime.NumCPU())

// 	cmd := strings.Join(flag.Args(), " ")

// 	apps := NewFastCGIAppServer(".", cmd, 4, false)
// 	srv := &http.Server{
// 		Addr:    ":8123",
// 		Handler: apps,
// 	}
// 	srv.ListenAndServe()
// }
