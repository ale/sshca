// FastCGI client.
//
// Based on bitbucket.org/PinIdea/fcgi_client, refactored to support
// request multiplexing and memory buffer recycling (both major
// performance boosts).
//
package fcgiclient

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/http/httputil"
	"net/textproto"
	"strconv"
	"sync"
)

const maxDataLen = 65536

const FCGI_LISTENSOCK_FILENO uint8 = 0
const FCGI_HEADER_LEN uint8 = 8
const VERSION_1 uint8 = 1
const FCGI_NULL_REQUEST_ID uint8 = 0
const FCGI_KEEP_CONN uint8 = 1

const (
	FCGI_BEGIN_REQUEST uint8 = iota + 1
	FCGI_ABORT_REQUEST
	FCGI_END_REQUEST
	FCGI_PARAMS
	FCGI_STDIN
	FCGI_STDOUT
	FCGI_STDERR
	FCGI_DATA
	FCGI_GET_VALUES
	FCGI_GET_VALUES_RESULT
	FCGI_UNKNOWN_TYPE
	FCGI_MAXTYPE = FCGI_UNKNOWN_TYPE
)

const (
	FCGI_RESPONDER uint8 = iota + 1
	FCGI_AUTHORIZER
	FCGI_FILTER
)

const (
	FCGI_REQUEST_COMPLETE uint8 = iota
	FCGI_CANT_MPX_CONN
	FCGI_OVERLOADED
	FCGI_UNKNOWN_ROLE
)

const (
	FCGI_MAX_CONNS  string = "MAX_CONNS"
	FCGI_MAX_REQS   string = "MAX_REQS"
	FCGI_MPXS_CONNS string = "MPXS_CONNS"
)

type memPool struct {
	ch      chan []byte
	bufsize int
}

func newMemPool(bufsize, poolsize int) *memPool {
	return &memPool{
		bufsize: bufsize,
		ch:      make(chan []byte, poolsize),
	}
}

func (m *memPool) Get() []byte {
	select {
	case buf := <-m.ch:
		return buf
	default:
		return make([]byte, m.bufsize)
	}
}

func (m *memPool) Release(buf []byte) {
	select {
	case m.ch <- buf:
	default:
	}
}

var mempool *memPool

func init() {
	mempool = newMemPool(maxDataLen+8, 256)
}

type record interface {
	RequestId() uint16
	RecordType() uint8
	Data() []byte
	Encode(buf *bytes.Buffer) ([]byte, error)
	Close()
	String() string
}

type header struct {
	Version       uint8
	Type          uint8
	Id            uint16
	ContentLength uint16
	PaddingLength uint8
	Reserved      uint8
}
type rawRecord struct {
	header
	data []byte
}

var rpad, wpad [256]byte

func newRawRecord(recType uint8, reqId uint16, data []byte, sz int) record {
	padding := (8 - (sz%8)%8)
	return &rawRecord{
		header: header{
			Version:       1,
			Type:          recType,
			Id:            reqId,
			ContentLength: uint16(sz),
			PaddingLength: uint8(padding),
		},
		data: data,
	}
}

func readRecord(r io.Reader) (record, error) {
	var rec rawRecord
	if err := binary.Read(r, binary.BigEndian, &rec.header); err != nil {
		return nil, err
	}
	if rec.Version != 1 {
		return nil, errors.New("invalid header version")
	}
	if rec.ContentLength > 0 {
		rec.data = mempool.Get()
		n, err := r.Read(rec.data[:rec.ContentLength])
		if err != nil {
			return nil, err
		} else if n < int(rec.ContentLength) {
			return nil, errors.New("short data")
		}
	}
	if rec.PaddingLength > 0 {
		n, err := r.Read(rpad[:rec.PaddingLength])
		if err != nil {
			return nil, err
		} else if n < int(rec.PaddingLength) {
			return nil, errors.New("short padding")
		}
	}
	return &bufBackedRecord{&rec}, nil
}

func (r *rawRecord) RequestId() uint16 {
	return r.Id
}

func (r *rawRecord) RecordType() uint8 {
	return r.Type
}

func (r *rawRecord) Data() []byte {
	if r.data == nil || r.ContentLength == 0 {
		return nil
	}
	return r.data[:r.ContentLength]
}

func (r *rawRecord) Close() {
}

func (r *rawRecord) String() string {
	return fmt.Sprintf("%#v", r.header)
}

func (r *rawRecord) Encode(buf *bytes.Buffer) ([]byte, error) {
	buf.Reset()
	if err := binary.Write(buf, binary.BigEndian, r.header); err != nil {
		return nil, err
	}
	if _, err := buf.Write(r.data[:r.ContentLength]); err != nil {
		return nil, err
	}
	if _, err := buf.Write(wpad[:r.PaddingLength]); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type bufBackedRecord struct {
	*rawRecord
}

func (r *bufBackedRecord) Close() {
	if r.data != nil {
		mempool.Release(r.data)
	}
}

func beginRequestRecord(reqId uint16) record {
	var role uint8 = FCGI_RESPONDER
	var flags uint8 = FCGI_KEEP_CONN
	b := [8]byte{byte(role >> 8), byte(role), flags}
	return newRawRecord(FCGI_BEGIN_REQUEST, reqId, b[:], 8)
}

func endRequestRecord(reqId uint16, appStatus int, protocolStatus uint8) record {
	b := make([]byte, 8)
	binary.BigEndian.PutUint32(b, uint32(appStatus))
	b[4] = protocolStatus
	return newRawRecord(FCGI_END_REQUEST, reqId, b, 8)
}

type streamRecordWriter struct {
	conn    *Conn
	recType uint8
	reqId   uint16
}

func newStreamRecordWriter(c *Conn, recType uint8, reqId uint16) *streamRecordWriter {
	return &streamRecordWriter{
		conn:    c,
		recType: recType,
		reqId:   reqId,
	}
}

func (s *streamRecordWriter) Write(p []byte) (int, error) {
	nn := 0
	for len(p) > 0 {
		n := len(p)
		if n > maxDataLen {
			n = maxDataLen
		}
		if err := s.conn.writeRecord(newRawRecord(s.recType, s.reqId, p[:n], n)); err != nil {
			return nn, err
		}
		nn += n
		p = p[n:]
	}
	return nn, nil
}

func (s *streamRecordWriter) Close() error {
	// Empty record to terminate the stream.
	return s.conn.writeRecord(newRawRecord(s.recType, s.reqId, nil, 0))
}

type bufferedRecordWriter struct {
	*streamRecordWriter
	buf []byte
	pos int
}

func newBufferedRecordWriter(c *Conn, recType uint8, reqId uint16) *bufferedRecordWriter {
	return &bufferedRecordWriter{
		streamRecordWriter: &streamRecordWriter{
			conn:    c,
			recType: recType,
			reqId:   reqId,
		},
		buf: mempool.Get(),
	}
}

func (s *bufferedRecordWriter) Write(p []byte) (int, error) {
	nn := 0
	for len(p) > 0 {
		n := maxDataLen - s.pos
		if n > len(p) {
			n = len(p)
		}
		copy(s.buf[s.pos:s.pos+n], p[:n])
		p = p[n:]
		nn += n
		s.pos += n
		if s.pos >= maxDataLen {
			if err := s.flush(); err != nil {
				return nn, err
			}
		}
	}
	return nn, nil
}

func (s *bufferedRecordWriter) flush() error {
	if s.pos > 0 {
		err := s.conn.writeRecord(newRawRecord(s.recType, s.reqId, s.buf[:s.pos], s.pos))
		s.pos = 0
		return err
	}
	return nil
}

func (s *bufferedRecordWriter) Close() error {
	s.flush()
	mempool.Release(s.buf)
	return s.streamRecordWriter.Close()
}

func encodeSize(b []byte, size uint32) int {
	if size > 127 {
		size |= 1 << 31
		binary.BigEndian.PutUint32(b, size)
		return 4
	}
	b[0] = byte(size)
	return 1
}

type Conn struct {
	lock sync.Mutex
	rwc  io.ReadWriteCloser

	reqCh  chan *responseReader
	doneCh chan uint16
}

func (c *Conn) Close() error {
	c.rwc.Close()
	close(c.reqCh)
	close(c.doneCh)
	return nil
}

type responseReader struct {
	id     uint16
	dataCh chan record
	doneCh chan<- uint16

	curRec record
	curBuf []byte
}

func (r *responseReader) nextRecord() error {
	for {
		rec := <-r.dataCh
		if rec == nil {
			log.Printf("empty record from channel")
			return io.EOF
		}

		switch rec.RecordType() {
		case FCGI_STDERR:
			log.Printf("request %d stderr: %s", r.id, string(rec.Data()))
			continue
		case FCGI_STDOUT:
			if r.curRec != nil {
				r.curRec.Close()
			}
			r.curRec = rec
			r.curBuf = rec.Data()
		case FCGI_END_REQUEST:
			return io.EOF
		}
		return nil
	}
}

func (r *responseReader) Close() error {
	if r.curRec != nil {
		r.curRec.Close()
	}
	r.doneCh <- r.id
	return nil
}

func (r *responseReader) Read(out []byte) (int, error) {
	nn := 0
	for len(out) > 0 {
		if len(r.curBuf) == 0 {
			if err := r.nextRecord(); err != nil {
				return nn, err
			}
		}
		n := len(out)
		if n > len(r.curBuf) {
			n = len(r.curBuf)
		}
		copy(out[:n], r.curBuf[:n])
		r.curBuf = r.curBuf[n:]
		out = out[n:]
		nn += n
	}
	return nn, nil
}

func (c *Conn) newResponseReader() *responseReader {
	reqId := uint16(rand.Int63n(1<<16 - 1))
	return &responseReader{
		id:     reqId,
		dataCh: make(chan record, 100),
		doneCh: c.doneCh,
	}
}

func (c *Conn) demux() {
	recCh := make(chan record, 100)
	responseMap := make(map[uint16]*responseReader)

	go func() {
		defer close(recCh)
		for {
			rec, err := readRecord(c.rwc)
			if err == io.EOF {
				break
			} else if err != nil {
				//log.Printf("connection error: %v", err)
				break
			}
			recCh <- rec
		}
	}()

	for {
		select {
		case rec := <-recCh:
			if rec == nil {
				//log.Printf("connection closed by remote peer")
				recCh = nil
				continue
			}

			rr, ok := responseMap[rec.RequestId()]
			if !ok {
				log.Printf("connection received reply for unknown request id %d: %s", rec.RequestId(), rec)
				continue
			}
			rr.dataCh <- rec

		case rr := <-c.reqCh:
			if rr == nil {
				return
			}
			if recCh == nil {
				close(rr.dataCh)
			}
			responseMap[rr.id] = rr

		case reqId := <-c.doneCh:
			if reqId == 0 {
				return
			}
			rr, ok := responseMap[reqId]
			if !ok {
				log.Printf("connection received termination for unknown request id %d", reqId)
				continue
			}
			close(rr.dataCh)
			delete(responseMap, reqId)
		}
	}
}

func (c *Conn) Request(p map[string]string, data io.Reader) (resp io.ReadCloser, err error) {

	rr := c.newResponseReader()

	c.reqCh <- rr

	if err := c.writeRecord(beginRequestRecord(rr.id)); err != nil {
		return nil, err
	}
	if err := c.writePairs(rr.id, p); err != nil {
		return nil, err
	}
	if err := c.writeStream(FCGI_STDIN, rr.id, data); err != nil {
		return nil, err
	}

	return rr, nil
}

type responseCloser struct {
	io.ReadCloser
	c io.Closer
}

func (c *responseCloser) Close() error {
	c.ReadCloser.Close()
	return c.c.Close()
}

func (c *Conn) HttpRequest(p map[string]string, data io.Reader) (resp *http.Response, err error) {
	r, err := c.Request(p, data)
	if err != nil {
		return
	}

	rb := bufio.NewReader(r)
	tp := textproto.NewReader(rb)
	resp = new(http.Response)

	// Parse the response headers.
	mimeHeader, err := tp.ReadMIMEHeader()
	if err != nil {
		return
	}
	resp.Header = http.Header(mimeHeader)

	// TODO: fixTransferEncoding ?
	resp.TransferEncoding = resp.Header["Transfer-Encoding"]
	resp.ContentLength, _ = strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 64)

	var respBody io.ReadCloser
	if chunked(resp.TransferEncoding) {
		respBody = ioutil.NopCloser(httputil.NewChunkedReader(rb))
	} else {
		respBody = ioutil.NopCloser(rb)
	}
	resp.Body = &responseCloser{respBody, r}

	return
}

func chunked(te []string) bool {
	return len(te) > 0 && te[0] == "chunked"
}

func (c *Conn) writeRecord(rec record) error {
	buf := mempool.Get()
	defer mempool.Release(buf)
	b := bytes.NewBuffer(buf)

	c.lock.Lock()
	data, err := rec.Encode(b)
	if err == nil {
		_, err = c.rwc.Write(data)
	}
	c.lock.Unlock()

	rec.Close()

	return err
}

func (c *Conn) writePairs(reqId uint16, p map[string]string) error {
	b := newBufferedRecordWriter(c, FCGI_PARAMS, reqId)
	defer b.Close()
	szbuf := make([]byte, 8)

	for k, v := range p {
		n := encodeSize(szbuf, uint32(len(k)))
		n += encodeSize(szbuf[n:], uint32(len(v)))
		if _, err := b.Write(szbuf[:n]); err != nil {
			return err
		}
		if _, err := io.WriteString(b, k); err != nil {
			return err
		}
		if _, err := io.WriteString(b, v); err != nil {
			return err
		}
	}
	return nil
}

func (c *Conn) writeStream(recType uint8, reqId uint16, r io.Reader) error {
	s := newStreamRecordWriter(c, recType, reqId)
	defer s.Close()
	_, err := io.Copy(s, r)
	return err
}

func Dial(network, address string) (fcgi *Conn, err error) {
	var conn net.Conn

	conn, err = net.Dial(network, address)
	if err != nil {
		return
	}

	fcgi = &Conn{
		rwc:    conn,
		reqCh:  make(chan *responseReader, 50),
		doneCh: make(chan uint16, 50),
	}
	go fcgi.demux()

	return
}
