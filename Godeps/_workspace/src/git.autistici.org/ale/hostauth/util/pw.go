package util

import (
	"os/user"
	"strconv"
)

func GetUser(uid int) string {
	username := strconv.Itoa(uid)
	if u, err := user.LookupId(username); err == nil {
		username = u.Username
	}
	return username
}
