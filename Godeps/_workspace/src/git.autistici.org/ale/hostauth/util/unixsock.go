package util

import (
	"encoding/gob"
	"log"
	"math/rand"
	"net"
	"os"
	"sync"
	"syscall"
)

type UnixCredentials struct {
	Pid  int32
	User string
}

func newUnixCredentials(ucred *syscall.Ucred) *UnixCredentials {
	return &UnixCredentials{
		Pid:  ucred.Pid,
		User: GetUser(int(ucred.Uid)),
	}
}

type UnixHandler interface {
	HandleUNIX(*Conn)
}

type UnixHandlerFunc func(*Conn)

func (f UnixHandlerFunc) HandleUNIX(conn *Conn) {
	f(conn)
}

type UnixServer struct {
	UnixHandler

	listener net.Listener
	wg       sync.WaitGroup
	ready    chan bool
}

func NewUnixServer(h UnixHandler) *UnixServer {
	return &UnixServer{
		UnixHandler: h,
		ready:       make(chan bool),
	}
}

func (u *UnixServer) WaitUntilReady() {
	<-u.ready
}

func (u *UnixServer) ListenAndServe(path string) error {
	os.Remove(path)

	addr, err := net.ResolveUnixAddr("unix", path)
	if err != nil {
		return err
	}
	l, err := net.ListenUnix("unix", addr)
	if err != nil {
		return err
	}
	defer l.Close()
	u.listener = l

	close(u.ready)

	for {
		conn, err := l.Accept()
		if err != nil {
			//log.Fatalf("Accept() error: %v", err)
			return err
		}
		u.wg.Add(1)
		go func(conn net.Conn) {
			defer u.wg.Done()
			defer conn.Close()

			rid := rand.Int63()
			uconn := conn.(*net.UnixConn)

			f, err := uconn.File()
			if err != nil {
				log.Fatalf("could not convert connection to file: %v", err)
			}
			ucred, err := syscall.GetsockoptUcred(int(f.Fd()), syscall.SOL_SOCKET, syscall.SO_PEERCRED)
			f.Close()
			if err != nil {
				log.Printf("%s: no peer credentials: %v", conn.RemoteAddr(), err)
				return
			}

			log.Printf("request %x from pid %d, uid %d", rid, ucred.Pid, ucred.Uid)

			u.HandleUNIX(&Conn{UnixConn: uconn, RequestID: rid, Credentials: newUnixCredentials(ucred)})
		}(conn)
	}
}

func (u *UnixServer) Close() {
	u.listener.Close()
	u.wg.Wait()
}

type Conn struct {
	*net.UnixConn
	RequestID   int64
	Credentials *UnixCredentials
}

func (c *Conn) ReadObj(obj interface{}) error {
	err := gob.NewDecoder(c).Decode(obj)
	c.CloseRead()
	return err
}

func (c *Conn) WriteObj(obj interface{}) error {
	err := gob.NewEncoder(c).Encode(obj)
	c.CloseWrite()
	return err
}

func DialUNIX(path string) (*Conn, error) {
	addr, err := net.ResolveUnixAddr("unix", path)
	if err != nil {
		return nil, err
	}
	uconn, err := net.DialUnix("unix", nil, addr)
	if err != nil {
		return nil, err
	}
	return &Conn{UnixConn: uconn}, nil
}
