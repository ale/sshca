package util

import (
	"net"
	"os"
	"strings"
)

var (
	Hostname  = "localhost"
	Hostnames []string
	LocalIPs  []net.IP
)

func localIPs() ([]net.IP, error) {
	var ips []net.IP
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}
	for _, a := range addrs {
		ip, _, err := net.ParseCIDR(a.String())
		if err != nil {
			continue
		}
		ips = append(ips, ip)
	}
	return ips, nil
}

func lookupIPs(ips []net.IP) []string {
	var out []string
	for _, ip := range ips {
		if !ip.IsGlobalUnicast() && !ip.IsLoopback() {
			continue
		}
		names, err := net.LookupAddr(ip.String())
		if err != nil {
			continue
		}
		for _, n := range names {
			out = append(out, strings.TrimSuffix(n, "."))
		}
	}
	return out
}

func localHostNames(localips []net.IP) ([]string, error) {
	tmp := make(map[string]struct{})
	if hostname, err := os.Hostname(); err == nil {
		tmp[hostname] = struct{}{}
	}
	for _, name := range lookupIPs(localips) {
		tmp[name] = struct{}{}
	}
	var out []string
	for name := range tmp {
		out = append(out, name)
	}
	return out, nil
}

func init() {
	if host, err := os.Hostname(); err == nil {
		Hostname = host
	}

	LocalIPs, _ = localIPs()
	Hostnames, _ = localHostNames(LocalIPs)
}
