package util

import (
	"fmt"
	"net/http"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/github.com/eapache/go-resiliency/retrier"
)

type HttpError struct {
	*http.Response
}

func NewHttpError(resp *http.Response) error {
	return &HttpError{resp}
}

func (err *HttpError) Error() string {
	return fmt.Sprintf("HTTP status %s", err.Status)
}

func (err *HttpError) Temporary() bool {
	return (err.StatusCode >= 500 && err.StatusCode < 600)
}

type TemporaryError struct {
	error
}

func (err *TemporaryError) Temporary() bool {
	return true
}

func NewTemporaryError(err error) error {
	return &TemporaryError{err}
}

type hasTemporary interface {
	Temporary() bool
}

type tmpClassifier struct{}

func (h *tmpClassifier) Classify(err error) retrier.Action {
	if err == nil {
		return retrier.Succeed
	}

	// Is it a temporary error?
	if terr, ok := err.(hasTemporary); ok && terr.Temporary() {
		return retrier.Retry
	}
	return retrier.Fail
}

func RetryWithBackoff() *retrier.Retrier {
	r := retrier.New(
		retrier.ExponentialBackoff(50, 50*time.Millisecond),
		&tmpClassifier{},
	)
	r.SetJitter(0.2)
	return r
}
