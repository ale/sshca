package util

import (
	"container/heap"
	"time"
)

type schedItem struct {
	name     string
	deadline time.Time
	index    int
}

type schedQueue []*schedItem

func (eq schedQueue) Len() int { return len(eq) }

func (eq schedQueue) Less(i, j int) bool {
	return eq[i].deadline.Before(eq[j].deadline)
}

func (eq schedQueue) Swap(i, j int) {
	eq[i], eq[j] = eq[j], eq[i]
	eq[i].index = i
	eq[j].index = j
}

func (eq *schedQueue) Push(x interface{}) {
	n := len(*eq)
	item := x.(*schedItem)
	item.index = n
	*eq = append(*eq, item)
}

func (eq *schedQueue) Pop() interface{} {
	old := *eq
	n := len(old)
	item := old[n-1]
	item.index = -1
	*eq = old[0 : n-1]
	return item
}

func (eq *schedQueue) update(item *schedItem) {
	heap.Fix(eq, item.index)
}

type Scheduler struct {
	queue schedQueue
	items map[string]*schedItem

	// Channel for updates.
	update chan *schedItem

	// Items are sent to this channel.
	C chan string
}

func NewScheduler() *Scheduler {
	s := &Scheduler{
		queue:  make(schedQueue, 0),
		items:  make(map[string]*schedItem),
		update: make(chan *schedItem, 10),
		C:      make(chan string, 10),
	}
	go s.run()
	return s
}

func (s *Scheduler) Add(name string, deadline time.Time) {
	s.update <- &schedItem{name: name, deadline: deadline}
}

func (s *Scheduler) Close() {
	close(s.update)
}

func (s *Scheduler) run() {
	var timer *time.Timer
	var timerCh <-chan time.Time
	var curItem *schedItem
	for {
		select {
		case item := <-s.update:
			if item == nil {
				close(s.C)
				return
			}
			if curItem != nil {
				heap.Push(&s.queue, curItem)
			}
			if oldItem, ok := s.items[item.name]; ok {
				oldItem.deadline = item.deadline
				s.queue.update(oldItem)
			} else {
				s.items[item.name] = item
				heap.Push(&s.queue, item)
			}
		case <-timerCh:
			if curItem != nil {
				s.C <- curItem.name
				delete(s.items, curItem.name)
			}
			curItem = nil
		}

		if s.queue.Len() > 0 {
			curItem = heap.Pop(&s.queue).(*schedItem)
			if timer != nil {
				timer.Stop()
			}
			delay := curItem.deadline.Sub(time.Now())
			timer = time.NewTimer(delay)
			timerCh = timer.C
		} else {
			timer = nil
			timerCh = nil
		}
	}
}
