package server

import (
	"crypto/x509"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/root"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

// Sign a certificate for ourselves (for when we need a client
// certificate).
type selfSignerProto struct {
	srv *Server
}

func (s *selfSignerProto) Sign(req *local.SignRequest, resp *local.SignResponse) error {
	s.srv.Sign(req, resp, nil)
	return nil
}

// Host key exchange server. Generates signed local user credentials
// using the host-specific intermediate CA key.
//
// User credentials are verified by checking SO_PEERCRED on the
// incoming connection via a UNIX socket. The server also listens on a
// public HTTP port broadcasting its public key.
//
type Server struct {
	*hostauth.CA

	stop    chan bool
	client  *client.Client
	renewer root.Renewer

	Update chan *x509.Certificate
}

func NewServer(ca *hostauth.CA, roots *x509.CertPool, rootCaAddr string) (*Server, error) {
	s := &Server{
		CA:     ca,
		stop:   make(chan bool),
		Update: make(chan *x509.Certificate),
	}

	// Prevent starting a Server with an expired certificate.
	if s.CA.Certificate.NotAfter.Before(time.Now().UTC()) {
		return nil, errors.New("Host certificate expired. Run `pki-hostd --init' to regenerate.")
	}

	// Create a client which short-circuits remote requests to the
	// Server object itself, avoiding RPC.
	c, err := client.NewClient(&selfSignerProto{s}, s, roots)
	if err != nil {
		return nil, err
	}
	s.client = c

	if rootCaAddr != "" {
		s.renewer = root.NewRootCAStub(rootCaAddr, c)
		go s.renew()
	}

	return s, nil
}

func (s *Server) Close() {
	close(s.stop)
	s.client.Close()
}

func (s *Server) renew() {
	defer close(s.Update)
	for {
		expiry := hostauth.ExpirationDeadline(s.Certificate)
		c := time.After(expiry.Sub(time.Now()))
		select {
		case <-s.stop:
			return
		case <-c:
		}

		log.Printf("host certificate about to expire, renewing...")
		crt, err := s.renewer.Renew(s.Certificate, s.PrivateKey)
		if err != nil {
			log.Fatal("ERROR: could not renew host certificate: %v", err)
		}
		s.Certificate = crt
		s.Update <- crt
	}
}

func (s *Server) GetHostCertificate() (*x509.Certificate, error) {
	return s.Certificate, nil
}

func (ca *Server) Sign(req *local.SignRequest, resp *local.SignResponse, transportCreds *util.UnixCredentials) {
	// Extract credentials from the CSR.
	csr, err := x509.ParseCertificateRequest(req.CertificateRequest)
	if err != nil {
		resp.Error = fmt.Sprintf("could not parse CSR: %v", err)
		return
	}
	creds, err := hostauth.NewCredentialsFromName(csr.Subject)
	if err != nil {
		resp.Error = fmt.Sprintf("could not parse CSR subject: %v", err)
		return
	}

	// Verify the credentials in the request against those
	// determined by the transport.
	if creds.Host != util.Hostname {
		resp.Error = fmt.Sprintf("bad credentials: host mismatch: got=%s, want=%s", creds.Host, util.Hostname)
		return
	}
	if transportCreds != nil && creds.User != transportCreds.User {
		resp.Error = fmt.Sprintf("bad credentials: user mismatch: got=%s, want=%s", creds.User, transportCreds.User)
		return
	}

	// Sign a new certificate with the credentials.
	var crt *x509.Certificate
	if req.IsServer {
		crt, err = ca.CreateServerCert(csr)
	} else {
		crt, err = ca.CreateClientCert(csr)
	}
	if err != nil {
		resp.Error = fmt.Sprintf("error signing certificate: %v", err)
		return
	}
	resp.PublicKey = crt.Raw
	if !req.IsServer {
		// For client usage, create a signed identity
		// statement as well.
		tkt := &hostauth.Ticket{
			Credentials: creds,
			Deadline:    crt.NotAfter,
		}
		resp.AuthTicket, err = tkt.Sign(ca.PrivateKey)
		if err != nil {
			log.Printf("error signing auth token: %v", err)
		}
	}
}

func (ca *Server) HandleUNIX(conn *util.Conn) {
	var req local.SignRequest
	var resp local.SignResponse
	if err := conn.ReadObj(&req); err != nil {
		log.Printf("request %x: error: %v", conn.RequestID, err)
		return
	}

	ca.Sign(&req, &resp, conn.Credentials)

	// Logging.
	if resp.Error != "" {
		log.Printf("request %x: error: %s", conn.RequestID, resp.Error)
	} else {
		// For debug purposes, dump the certificate details.
		crt, _ := x509.ParseCertificate(resp.PublicKey)
		log.Printf("request %x: signed certificate %s", conn.RequestID, hostauth.CertificateToString(crt))
	}

	if err := conn.WriteObj(&resp); err != nil {
		log.Printf("request %x: write error: %v", conn.RequestID, err)
	}
}

func (ca *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	der := ca.Certificate.Raw
	w.Header().Set("Content-Type", "application/x-x509-ca-cert")
	w.Header().Set("Content-Length", strconv.Itoa(len(der)))
	w.Write(der)
}
