package local

import (
	"crypto/ecdsa"
	"crypto/x509"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

var hostdPort = flag.Int("authd-hostd-port", 2024, "port where hostd is running")

type SignRequest struct {
	CertificateRequest []byte `json:"csr"`
	IsServer           bool   `json:"is_server"`
	IsCA               bool   `json:"is_ca"`
	Validity           int    `json:"validity"` // seconds
}

type SignResponse struct {
	PublicKey  []byte `json:"public_key"`
	AuthTicket string `json:"auth_ticket"`
	Error      string `json:"error,omitempty"`
}

type SignerProtocol interface {
	Sign(req *SignRequest, resp *SignResponse) error
}

type socketSigner struct {
	socketPath string
}

func (s *socketSigner) Sign(req *SignRequest, resp *SignResponse) error {
	// Connection and network issues are always considered
	// temporary. Only return a permanent error if we successfully
	// communicate with the server.
	return util.RetryWithBackoff().Run(func() error {
		conn, err := util.DialUNIX(s.socketPath)
		if err != nil {
			log.Printf("sign error: %v", err)
			return util.NewTemporaryError(err)
		}
		defer conn.Close()

		if err := conn.WriteObj(req); err != nil {
			log.Printf("sign error: %v", err)
			return util.NewTemporaryError(err)
		}
		if err := conn.ReadObj(resp); err != nil {
			log.Printf("sign error: %v", err)
			return util.NewTemporaryError(err)
		}
		return nil
	})
}

func NewSocketSignerProtocol(socketPath string) SignerProtocol {
	return &socketSigner{socketPath}
}

type Signer struct {
	proto SignerProtocol
}

func NewSigner(proto SignerProtocol) *Signer {
	return &Signer{proto: proto}
}

func (s *Signer) Sign(creds hostauth.Credentials, isServer bool, names []string, ips []net.IP) (*x509.Certificate, *ecdsa.PrivateKey, string, error) {
	var cn string
	if len(names) > 0 {
		cn = names[0]
	}
	csr, priv, err := hostauth.CreateCertificateRequest(creds.Name(cn), names, ips)
	if err != nil {
		return nil, nil, "", err
	}

	request := SignRequest{
		CertificateRequest: csr.Raw,
		IsServer:           isServer,
	}
	var response SignResponse

	if err := s.proto.Sign(&request, &response); err != nil {
		return nil, nil, "", err
	}
	if response.Error != "" {
		return nil, nil, "", errors.New(response.Error)
	}

	crt, err := x509.ParseCertificate(response.PublicKey)
	if err != nil {
		return nil, nil, "", err
	}
	tkt := response.AuthTicket

	return crt, priv, tkt, err
}

type HostCertGetter interface {
	GetHostCertificate() (*x509.Certificate, error)
}

type httpHostCertGetter struct {
	port  int
	roots *x509.CertPool
}

func newHttpHostCertGetter(port int, roots *x509.CertPool) *httpHostCertGetter {
	return &httpHostCertGetter{port: port, roots: roots}
}

const HostCertURL = "/_well_known/host.crt"

func (g *httpHostCertGetter) GetHostCertificate() (*x509.Certificate, error) {
	var crt *x509.Certificate

	url := fmt.Sprintf("http://localhost:%d%s", g.port, HostCertURL)
	err := util.RetryWithBackoff().Run(func() error {
		resp, err := http.Get(url)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			return util.NewHttpError(resp)
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		crt, err = x509.ParseCertificate(data)
		if err != nil {
			return err
		}

		// Verify that the remote host certificate is indeed
		// signed by the root CA.
		if !isSignedBy(crt, g.roots) {
			return errors.New("verification failed: certificate is not signed by the root CA")
		}

		return err
	})

	return crt, err
}

func DefaultHostCertGetter(roots *x509.CertPool) HostCertGetter {
	return newHttpHostCertGetter(*hostdPort, roots)
}

func NewHostCertGetter(port int, roots *x509.CertPool) HostCertGetter {
	return newHttpHostCertGetter(port, roots)
}

// isSignedBy returns true if the certificate is signed by the
// root certificates in the pool.
func isSignedBy(crt *x509.Certificate, roots *x509.CertPool) bool {
	_, err := crt.Verify(x509.VerifyOptions{
		Roots:     roots,
		KeyUsages: []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
	})
	return err == nil
}
