package root

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"net/http"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
	"golang.org/x/crypto/scrypt"
)

type SignRequest struct {
	CertificateRequest []byte `json:"csr"`
}

type SignResponse struct {
	PublicKey []byte `json:"public_key"`
	Salt      []byte `json:"salt"`
	Error     string `json:"error"`
}

type Client interface {
	ClientTlsConfig(string) (*tls.Config, error)
}

type Renewer interface {
	Renew(*x509.Certificate, *ecdsa.PrivateKey) (*x509.Certificate, error)
}

type Signer interface {
	Sign(string, func() ([]byte, error)) (*x509.Certificate, *ecdsa.PrivateKey, error)
}

type RootCA interface {
	Renewer
}

func jsonRequest(remoteUrl string, tlsConfig *tls.Config, req, resp interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return err
	}
	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	client := &http.Client{Transport: tr}
	hresp, err := client.Post(remoteUrl, "application/json", &buf)
	if err != nil {
		return util.NewTemporaryError(err)
	}
	defer hresp.Body.Close()

	if hresp.StatusCode != 200 {
		return util.NewTemporaryError(fmt.Errorf("HTTP status code %s", hresp.Status))
	}
	if ctype := hresp.Header.Get("Content-Type"); ctype != "application/json" {
		return util.NewTemporaryError(fmt.Errorf("unexpected Content-Type %s", ctype))
	}

	if err = json.NewDecoder(hresp.Body).Decode(resp); err != nil {
		return util.NewTemporaryError(err)
	}
	return nil
}

type rootCaSigner struct {
	addr  string
	roots *x509.CertPool
}

func NewRootCASignerStub(addr string, roots *x509.CertPool) Signer {
	return &rootCaSigner{
		addr:  addr,
		roots: roots,
	}
}

func (r *rootCaSigner) Sign(hostname string, passwordFn func() ([]byte, error)) (*x509.Certificate, *ecdsa.PrivateKey, error) {
	var crt *x509.Certificate

	csr, priv, err := hostauth.CreateCertificateRequest(pkix.Name{CommonName: hostname}, nil, nil)
	if err != nil {
		return nil, nil, err
	}
	request := &SignRequest{
		CertificateRequest: csr.Raw,
	}
	remoteUrl := fmt.Sprintf("https://%s/sign", r.addr)

	// Create a simple, tolerant TLS config.
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            r.roots,
	}

	err = util.RetryWithBackoff().Run(func() error {
		var response SignResponse
		if err := jsonRequest(remoteUrl, tlsConfig, request, &response); err != nil {
			return err
		}
		if response.Error != "" {
			return errors.New(response.Error)
		}

		// Decrypt the returned data.
		encBlock, _ := pem.Decode(response.PublicKey)
		if !x509.IsEncryptedPEMBlock(encBlock) {
			return errors.New("unencrypted response")
		}
		pw, err := passwordFn()
		if err != nil {
			return err
		}
		aesKey, _, err := GenKey(pw, response.Salt)
		if err != nil {
			return err
		}
		crtData, err := x509.DecryptPEMBlock(encBlock, aesKey)
		if err == x509.IncorrectPasswordError {
			return util.NewTemporaryError(err)
		} else if err != nil {
			return err
		}

		crt, err = x509.ParseCertificate(crtData)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return nil, nil, err
	}
	return crt, priv, nil
}

type rootCaRenewer struct {
	addr   string
	client Client
}

func NewRootCAStub(addr string, client Client) RootCA {
	return &rootCaRenewer{
		addr:   addr,
		client: client,
	}
}

func (r *rootCaRenewer) Renew(oldCrt *x509.Certificate, priv *ecdsa.PrivateKey) (*x509.Certificate, error) {
	var crt *x509.Certificate

	// Sign a new CSR with the current private key and the same
	// X509 subject as the current certificate.
	csr, err := hostauth.CreateCertificateRequestWithKey(oldCrt.Subject, nil, nil, priv)
	if err != nil {
		return nil, err
	}
	request := &SignRequest{
		CertificateRequest: csr.Raw,
	}
	remoteUrl := fmt.Sprintf("https://%s/renew", r.addr)

	err = util.RetryWithBackoff().Run(func() error {
		var response SignResponse
		tlsConfig, err := r.client.ClientTlsConfig(r.addr)
		if err != nil {
			return err
		}
		if err := jsonRequest(remoteUrl, tlsConfig, request, &response); err != nil {
			return err
		}
		if response.Error != "" {
			return errors.New(response.Error)
		}

		crt, err = x509.ParseCertificate(response.PublicKey)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return nil, err
	}
	return crt, nil
}

// GenKey runs scrypt on a password to obtain an AES-256 key.
func GenKey(pw, salt []byte) ([]byte, []byte, error) {
	if salt == nil {
		salt = make([]byte, 16)
		rand.Reader.Read(salt)
	}
	key, err := scrypt.Key(pw, salt, 16384, 8, 1, 32)
	return key, salt, err
}
