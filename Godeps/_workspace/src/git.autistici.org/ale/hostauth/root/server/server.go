package server

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/root"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

type Server struct {
	*hostauth.CA

	encryptionPw []byte
}

func NewServer(ca *hostauth.CA, encryptionPw []byte) (*Server, error) {
	return &Server{
		CA:           ca,
		encryptionPw: encryptionPw,
	}, nil
}

func (s *Server) Sign(req *root.SignRequest, resp *root.SignResponse) {
	csr, err := x509.ParseCertificateRequest(req.CertificateRequest)
	if err != nil {
		resp.Error = fmt.Sprintf("could not parse CSR: %v", err)
		return
	}

	crt, err := s.CreateCA(csr)
	if err != nil {
		resp.Error = fmt.Sprintf("error signing certificate: %v", err)
		return
	}

	aesKey, salt, err := root.GenKey(s.encryptionPw, nil)
	if err != nil {
		resp.Error = fmt.Sprintf("error encrypting certificate: %v", err)
		return
	}

	block, err := x509.EncryptPEMBlock(rand.Reader, "CERTIFICATE", crt.Raw, aesKey, x509.PEMCipherAES256)
	if err != nil {
		resp.Error = fmt.Sprintf("error encrypting certificate: %v", err)
		return
	}
	resp.PublicKey = pem.EncodeToMemory(block)
	resp.Salt = salt
}

func (s *Server) Renew(req *root.SignRequest, resp *root.SignResponse, connState *tls.ConnectionState) {
	csr, err := x509.ParseCertificateRequest(req.CertificateRequest)
	if err != nil {
		resp.Error = fmt.Sprintf("could not parse CSR: %v", err)
		return
	}

	// Check that the CSR is signed by the host certificate used
	// in the connection. The host certificate is the second one
	// in the first (and only) verified trust chain.
	if len(connState.VerifiedChains) < 1 {
		resp.Error = "no verified trust chain"
		return
	}
	vchain := connState.VerifiedChains[0]
	if len(vchain) != 3 {
		resp.Error = "verification chain has unexpected length"
		return
	}
	oldCert := vchain[1]

	if err := oldCert.CheckSignature(csr.SignatureAlgorithm, csr.RawTBSCertificateRequest, csr.Signature); err != nil {
		resp.Error = fmt.Sprintf("error verifying CSR signature: %v", err)
		return
	}

	crt, err := s.CreateCA(csr)
	if err != nil {
		resp.Error = fmt.Sprintf("error signing certificate: %v", err)
		return
	}
	resp.PublicKey = crt.Raw
}

// Log something with an HTTP request context.
func logRequest(r *http.Request, format string, args ...interface{}) {
	log.Printf("%s %s %s - %s", r.RemoteAddr, r.Method, r.URL.Path, fmt.Sprintf(format, args...))
}

// Log high-level information about an operation.
func logOp(op string, r *http.Request, req *root.SignRequest, resp *root.SignResponse) {
	subj := "unknown"
	if csr, err := x509.ParseCertificateRequest(req.CertificateRequest); err == nil {
		subj = hostauth.CertificateRequestToString(csr)
	}
	if resp.Error != "" {
		logRequest(r, "%s %s: error: %s", op, subj, resp.Error)
	} else {
		logRequest(r, "%s %s: OK", op, subj)
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.TLS == nil {
		http.Error(w, "Bad protocol", http.StatusBadRequest)
		logRequest(r, "bad protocol")
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		logRequest(r, "bad request")
		return
	}

	if r.URL.Path == "/renew" {
		var req root.SignRequest
		var resp root.SignResponse
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			logRequest(r, "bad request: %v", err)
			return
		}
		s.Renew(&req, &resp, r.TLS)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&resp)
		logOp("renew", r, &req, &resp)
	} else if r.URL.Path == "/sign" {
		var req root.SignRequest
		var resp root.SignResponse
		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			logRequest(r, "bad request: %v", err)
			return
		}
		s.Sign(&req, &resp)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(&resp)
		logOp("sign", r, &req, &resp)
	} else {
		http.NotFound(w, r)
		logRequest(r, "not found")
	}
}

func (s *Server) TlsConfig() (*tls.Config, error) {
	// Create a long-term certificate just for the HTTP API.
	// TODO: autorenewal.
	// TODO: fix alt hostnames.
	csr, priv, err := hostauth.CreateCertificateRequest(pkix.Name{CommonName: util.Hostname}, nil, nil)
	if err != nil {
		return nil, err
	}
	crt, err := s.CreateServerCert(csr)
	if err != nil {
		return nil, err
	}

	pool := x509.NewCertPool()
	pool.AddCert(s.Certificate)

	tlsconf := &tls.Config{
		Certificates: []tls.Certificate{{
			Certificate: [][]byte{crt.Raw},
			PrivateKey:  priv,
			Leaf:        crt,
		}},
		MinVersion:               tls.VersionTLS12,
		CipherSuites:             hostauth.CipherSuites,
		PreferServerCipherSuites: true,
		ClientAuth:               tls.VerifyClientCertIfGiven,
		ClientCAs:                pool,
		NextProtos:               []string{"http/1.1"},
	}

	return tlsconf, nil
}

func (s *Server) ListenAndServe(addr string) error {
	// Build our own TLS listener using the temporary server
	// certificate and opportunistic client verification.
	tlsconf, err := s.TlsConfig()
	if err != nil {
		return err
	}
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	tl := tls.NewListener(util.TCPKeepAliveListener{l.(*net.TCPListener)}, tlsconf)

	srv := &http.Server{
		Handler:      s,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}
	return srv.Serve(tl)
}
