package hostauth

import (
	"crypto/x509/pkix"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

var execName string

func init() {
	execName = filepath.Base(os.Args[0])
}

// The identity credentials that are validated by hostauth. These
// credentials can be encoded as the Subject property of a X509
// certificate. The mapping of credentials fields to Subject fields is
// somewhat arbitrary, with one exception: the CN field is set
// separately, in order to satisfy the SSL library hostname validation
// criteria (so for servers it will be set to the listening address).
//
// Application certificates are distinguished from infrastructure
// certificates with the X509 key usage flags: infrastructure
// certificates have the CA bit set and cannot be used for encryption.
type Credentials struct {
	User    string
	Host    string
	Service string
}

func NewCredentials() Credentials {
	return Credentials{
		User:    util.GetUser(os.Getuid()),
		Host:    util.Hostname,
		Service: execName,
	}
}

func NewCredentialsFromName(name pkix.Name) (Credentials, error) {
	var creds Credentials
	if len(name.Locality) != 1 {
		return creds, errors.New("invalid credentials (missing host)")
	}
	if len(name.OrganizationalUnit) != 1 {
		return creds, errors.New("invalid credentials (missing user)")
	}
	if len(name.Organization) != 1 {
		return creds, errors.New("invalid credentials (missing service)")
	}
	creds.Service = name.Organization[0]
	creds.User = name.OrganizationalUnit[0]
	creds.Host = name.Locality[0]
	return creds, nil
}

func (c Credentials) Name(cn string) pkix.Name {
	if cn == "" {
		cn = c.User
	}
	return pkix.Name{
		CommonName:         cn,
		Organization:       []string{c.Service},
		OrganizationalUnit: []string{c.User},
		Locality:           []string{c.Host},
	}
}

func (c Credentials) String() string {
	return fmt.Sprintf("%s/%s@%s", c.User, c.Service, c.Host)
}

// Return a pkix.Name suitable for a host CA.
func HostCAName(hostname string) pkix.Name {
	return pkix.Name{
		Locality:   []string{hostname},
		CommonName: hostname,
	}
}
