package main

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
)

var (
	isServer   = flag.Bool("server", false, "create a server certificate")
	serverAddr = flag.String("server-addr", "", "server address")
	isClient   = flag.Bool("client", false, "create a client certificate (default)")
	service    = flag.String("service", "", "service name for certificate credentials")
	jsonOutput = flag.Bool("json", false, "JSON output")
	outCrtFile = flag.String("out-crt", "out.crt", "output certificate file (PEM)")
	outKeyFile = flag.String("out-key", "out.key", "output key file (PEM)")
)

type Output struct {
	Cert    []byte `json:"cert"`
	Key     []byte `json:"key"`
	Expires string `json:"expires"`
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Create hostauth SSL certificates for applications (either client /
server certs) by talking to the local pki-hostd server.

The new private key and X509 certificate are written on standard
output in JSON-encoded format, so that tools can consume the data
reliably.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if flag.NArg() > 0 {
		log.Fatal("Too many arguments. Run with `--help' for details.")
	}

	if *isServer && *isClient {
		log.Fatal("Must specify only one of --server or --client.")
	}
	if *isServer && *serverAddr == "" {
		*serverAddr = util.Hostname
	}
	if !*isServer && !*isClient {
		*isClient = true
	}

	c, err := client.New()
	if err != nil {
		log.Fatal(err)
	}
	if *service != "" {
		c.Creds.Service = *service
	}
	defer c.Close()

	var crt *x509.Certificate
	var priv interface{}
	if *isServer {
		crt, priv, _, err = c.ServerCertificate(*serverAddr)
	} else {
		crt, priv, err = c.ClientCertificate()
	}
	if err != nil {
		log.Fatal(err)
	}

	keyData, err := hostauth.MarshalPrivateKey(priv.(*ecdsa.PrivateKey))
	if err != nil {
		log.Fatal(err)
	}

	if *jsonOutput {
		out := Output{
			Cert:    crt.Raw,
			Key:     keyData,
			Expires: crt.NotAfter.Format(time.RFC3339),
		}
		enc, err := json.MarshalIndent(&out, "", "    ")
		if err != nil {
			log.Fatal(err)
		}
		os.Stdout.Write(enc)
		os.Stdout.WriteString("\n")
	} else {
		err = ioutil.WriteFile(
			*outKeyFile,
			hostauth.EncodePrivateKeyPEM(priv.(*ecdsa.PrivateKey)),
			0400)
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile(
			*outCrtFile,
			hostauth.EncodeCertificatePEM(crt),
			0644)
		if err != nil {
			log.Fatal(err)
		}
	}

	os.Exit(0)
}
