package main

import (
	"crypto/x509"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local/server"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/root"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
	"github.com/howeyc/gopass"
)

var (
	addr       = flag.String("addr", ":2024", "TCP port to listen on for public key requests")
	socketPath = flag.String("socket", "/var/run/hostauth/local", "UNIX socket to listen on")
	rootCaAddr = flag.String("rootca-addr", "", "address of pki-rootd")

	// Location of the host credentials.
	certPath = flag.String("host-crt", "/var/lib/hostauth/host.crt", "location of the host certificate (public)")
	keyPath  = flag.String("host-key", "/var/lib/hostauth/host.key", "location of the host key (private)")

	doInit       = flag.Bool("init", false, "initialize credentials and exit")
	certPassword = flag.String("init-cert-password", "", "password to decrypt initial credentials")

	clientCertTimeout = flag.Duration("client-cert-validity", 2*time.Hour, "validity of generated client certificates")
	serverCertTimeout = flag.Duration("server-cert-validity", 26*time.Hour, "validity of generated server certificates")
)

func loadCA() (*server.Server, error) {
	ca, err := hostauth.LoadCA(*keyPath, *certPath)
	if err != nil {
		return nil, err
	}

	return server.NewServer(ca, nil, *rootCaAddr)
}

func saveCA(crt *x509.Certificate) error {
	return ioutil.WriteFile(*certPath, hostauth.EncodeCertificatePEM(crt), 0644)
}

// Return the certificate decryption password (a shared secret with
// the root CA daemon). This can be specified as a command-line flag,
// or the environment variable DECRYPTION_PASSWORD, alternatively the
// user will be prompted for it interactively.
func askPass() ([]byte, error) {
	if *certPassword != "" {
		return []byte(*certPassword), nil
	}
	if p := os.Getenv("DECRYPTION_PASSWORD"); p != "" {
		return []byte(p), nil
	}
	fmt.Printf("Decryption password: ")
	pass := gopass.GetPasswdMasked()
	if len(pass) == 0 {
		return nil, errors.New("Abort")
	}
	return pass, nil
}

func initCA() error {
	roots, err := client.LoadRootPool()
	if err != nil {
		return err
	}
	rootca := root.NewRootCASignerStub(*rootCaAddr, roots)

	crt, priv, err := rootca.Sign(util.Hostname, askPass)
	if err != nil {
		return err
	}

	// Save crt and priv.
	if err := ioutil.WriteFile(*certPath, hostauth.EncodeCertificatePEM(crt), 0644); err != nil {
		return err
	}
	if err := ioutil.WriteFile(*keyPath, hostauth.EncodePrivateKeyPEM(priv), 0400); err != nil {
		return err
	}

	return nil
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Host CA daemon, signs certificate requests for local users.

It will automatically renew the host CA certificate before it expires.
If it is unable to contact the root CA service before the old
certificate expires, credentials will have to be renewed manually.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if *rootCaAddr == "" {
		log.Fatal("Must specify --rootca-addr. Run with `--help' for details.")
	}

	if *doInit {
		if err := initCA(); err != nil {
			log.Fatal(err)
		}
		return
	}

	ca, err := loadCA()
	if err != nil {
		log.Printf("ERROR: local host certificate not found (%v). Must initialize host credentials.", err)
		os.Exit(1)
	}
	ca.ClientCertTimeout = *clientCertTimeout
	ca.ServerCertTimeout = *serverCertTimeout

	unixSrv := util.NewUnixServer(ca)

	http.Handle(local.HostCertURL, ca)
	go func() {
		http.ListenAndServe(*addr, nil)
	}()

	termCh := make(chan os.Signal, 1)
	signal.Notify(termCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		s := <-termCh
		log.Printf("received signal %s, exiting", s)
		unixSrv.Close()
		os.Exit(0)
	}()

	go func() {
		for crt := range ca.Update {
			if err := saveCA(crt); err != nil {
				log.Printf("error saving new certificate: %v", err)
			}
		}
	}()

	syscall.Umask(0)

	// Lock all memory, prevent swapping credentials.
	// MCL_FUTURE -> 2, MCL_CURRENT -> 1.
	syscall.Mlockall(3)

	if err := unixSrv.ListenAndServe(*socketPath); err != nil {
		log.Fatal(err)
	}
}
