package main

import (
	"crypto/ecdsa"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
)

var (
	service  = flag.String("service", "", "service name (optional)")
	addr     = flag.String("addr", "", "address of the managed service")
	certPath = flag.String("crt", "", "location of the managed certificate")
	keyPath  = flag.String("key", "", "location of the managed private key")

	restartCmd string
)

func saveCert(crt, hostCrt *x509.Certificate, priv *ecdsa.PrivateKey) error {
	if err := ioutil.WriteFile(
		*certPath,
		hostauth.EncodeCertificatePEM(crt, hostCrt),
		0644,
	); err != nil {
		return err
	}

	if err := ioutil.WriteFile(
		*keyPath,
		hostauth.EncodePrivateKeyPEM(priv),
		0400,
	); err != nil {
		return err
	}

	return nil
}

func reload() {
	err := exec.Command("/bin/sh", "-c", restartCmd).Run()
	if err != nil {
		log.Printf("ERROR: service reload failed: %v", err)
	}
}

func watch(c *client.Client, update <-chan string) {
	for tag := range update {
		crt, priv, _, err := c.ServerCertificate(tag)
		if err != nil {
			log.Printf("ERROR: could not retrieve updated certificate: %v", err)
			continue
		}
		if err := saveCert(crt, c.HostCertificate(), priv.(*ecdsa.PrivateKey)); err != nil {
			log.Printf("ERROR: could not save new certificate: %v", err)
			continue
		}
		log.Printf("certificates updated, reloading service...")
		reload()
	}
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>] <COMMAND> [<ARGS>...]

Manage hostauth SSL certificates for a service.

This tool will generate a SSL server certificate and private key for a
service, renewing them whenever they are about to expire. Whenever the
certificates change, COMMAND is invoked to restart the service.

You must provide the address the service is listening on with the
'--addr' option in order to properly create the hostauth certificate.

The generated files will be encoded in the PEM format. The server
certificate includes the intermediate host CA.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if *addr == "" {
		log.Fatal("Must specify --addr. Run with `--help' for details.")
	}
	if *certPath == "" || *keyPath == "" {
		log.Fatal("Both --crt and --key must be specified. Run with `--help' for details.")
	}
	restartCmd = strings.Join(flag.Args(), " ")
	if restartCmd == "" {
		log.Fatal("Must specify the restart command. Run with `--help' for details.")
	}

	c, err := client.New()
	if err != nil {
		log.Fatal(err)
	}
	if *service != "" {
		c.Creds.Service = *service
	}
	defer c.Close()

	crt, priv, update, err := c.ServerCertificate(*addr)
	if err != nil {
		log.Fatal(err)
	}

	if err := saveCert(crt, c.HostCertificate(), priv.(*ecdsa.PrivateKey)); err != nil {
		log.Fatal(err)
	}

	watch(c, update)
}
