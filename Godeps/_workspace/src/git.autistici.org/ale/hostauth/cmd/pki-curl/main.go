package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
)

var (
	formStr = newFormData("F", "HTTP POST multipart form data")
	method  = flag.String("X", "GET", "HTTP method")
	dataStr = flag.String("d", "", "HTTP POST data")
)

type formData map[string][]string

func (f *formData) String() string {
	var sl []string
	for k, v := range *f {
		sl = append(sl, fmt.Sprintf("%s=%s", k, v))
	}
	return strings.Join(sl, ",")
}

func (f *formData) Set(value string) error {
	parts := strings.SplitN(value, "=", 2)
	if len(parts) != 2 {
		return errors.New("bad key=value format")
	}
	cur, ok := (*f)[parts[0]]
	if !ok {
		cur = []string{}
	}
	(*f)[parts[0]] = append(cur, parts[1])
	return nil
}

func newFormData(name, help string) *map[string][]string {
	var l formData
	flag.Var(&l, name, help)
	return (*map[string][]string)(&l)
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Serve static content from a directory over HTTP.

It will automatically renew the server certificate, and it allows
you to set an ACL to control access.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if flag.NArg() != 1 {
		log.Fatal("Wrong number of arguments. Run with `--help' for details.")
	}

	c, err := client.New()
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	urlStr := flag.Arg(0)
	u, _ := url.Parse(urlStr)
	tlsConf, err := c.ClientTlsConfig(u.Host)
	if err != nil {
		log.Fatal(err)
	}
	tr := &http.Transport{
		TLSClientConfig:    tlsConf,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	var reqBody io.Reader
	if len(*formStr) > 0 {
		values := make(url.Values)
		for k, vv := range *formStr {
			for _, v := range vv {
				values.Add(k, v)
			}
		}
		reqBody = strings.NewReader(values.Encode())
		*method = "POST"
	} else if *dataStr != "" {
		reqBody = strings.NewReader(*dataStr)
		*method = "POST"
	}

	req, err := http.NewRequest(*method, urlStr, reqBody)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
	}
	if resp.StatusCode > 300 {
		log.Fatalf("ERROR: HTTP status %v", resp.Status)
	}
	defer resp.Body.Close()

	io.Copy(os.Stdout, resp.Body)
}
