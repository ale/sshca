package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/root/server"
)

var (
	addr = flag.String("addr", ":2025", "TCP port to listen on for sign/renewal requests")

	secretPath      = flag.String("secret-file", "/etc/hostauth/root.secret", "file containing secret for certificate encryption")
	certPath        = flag.String("root-crt", "/var/lib/hostauth/root.crt", "location of the root CA certificate (public)")
	keyPath         = flag.String("root-key", "/var/lib/hostauth/root.key", "location of the root CA key (private)")
	hostCertTimeout = flag.Duration("cert-validity", 7*24*time.Hour, "validity of host intermediate CA certificates")
)

func loadCA(secret []byte) (*server.Server, error) {
	ca, err := hostauth.LoadCA(*keyPath, *certPath)
	if err != nil {
		return nil, err
	}
	return server.NewServer(ca, secret)
}

func loadSecret() ([]byte, error) {
	data, err := ioutil.ReadFile(*secretPath)
	if err != nil {
		return nil, err
	}
	return bytes.TrimSpace(data), nil
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Root CA daemon, signs and renews certificate requests for hosts. Run
'pki-catool --init' to generate the root CA certificates.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if flag.NArg() > 0 {
		log.Fatal("Too many arguments. Run with `--help' for details.")
	}

	secret, err := loadSecret()
	if err != nil {
		log.Fatal(err)
	}

	srv, err := loadCA(secret)
	if err != nil {
		log.Fatal(err)
	}
	srv.CaCertTimeout = *hostCertTimeout

	if err := srv.ListenAndServe(*addr); err != nil {
		log.Fatal(err)
	}
}
