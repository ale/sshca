package main

import (
	"crypto/ecdsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
)

var (
	caKeyFile  = flag.String("ca-key", "ca.key", "private key file")
	caCrtFile  = flag.String("ca-crt", "ca.crt", "public key file")
	outKeyFile = flag.String("out-key", "out.key", "output private key file")
	outCrtFile = flag.String("out-crt", "out.crt", "output public key file")
	doInit     = flag.Bool("init", false, "initialize self-signed root CA")

	doCA     = flag.Bool("ca", false, "create a CA certificate")
	doClient = flag.Bool("client", false, "create a client certificate")
	doServer = flag.Bool("server", false, "create a server certificate")
)

func b2i(b bool) int {
	if b {
		return 1
	}
	return 0
}

func saveCert(key *ecdsa.PrivateKey, crt *x509.Certificate, keyFile, crtFile string) error {
	if err := ioutil.WriteFile(
		crtFile,
		hostauth.EncodeCertificatePEM(crt),
		0644,
	); err != nil {
		return err
	}
	if err := ioutil.WriteFile(
		keyFile,
		hostauth.EncodePrivateKeyPEM(key),
		0400,
	); err != nil {
		return err
	}
	return nil
}

func saveCA(ca *hostauth.CA, keyFile, crtFile string) error {
	return saveCert(ca.PrivateKey, ca.Certificate, keyFile, crtFile)
}

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Create hostauth CA certificates offline.

Most operations require a CA certificate as input (specified using the
'--ca-key' and '--ca-crt' options), and will generate a certificate /
private key pair as output. Output location can be controlled using
the '--out-key' and '--out-crt' options.


Usage examples:

* Initialize the root CA certificate:

    $ pki-catool --init --out-key=root.key --out-crt=root.crt


* Create an intermediate (host) CA certificate for a given host:

    $ pki-catool --ca --ca-key=root.key --ca-crt=root.crt \
        --out-key=host.key --out-crt=host.crt $HOSTNAME


* Create a client certificate signed by the host CA:

    $ pki-catool --client --ca-key=host.key --ca-crt=host.crt


Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	log.SetFlags(0)
	flag.Parse()

	if (b2i(*doCA) + b2i(*doClient) + b2i(*doServer) + b2i(*doInit)) != 1 {
		log.Fatal("Must specify only one of --init, --ca, --client, --server. Run with `--help' for details.")
	}

	switch {

	case *doInit:
		ca, err := hostauth.CreateRootCA()
		if err != nil {
			log.Fatalf("Could not create root CA: %v", err)
		}
		if err := saveCA(ca, *outKeyFile, *outCrtFile); err != nil {
			log.Fatalf("Could not save output: %v", err)
		}

	case *doCA:
		if flag.NArg() != 1 {
			log.Fatal("Wrong number of arguments. Run with `--help' for details.")
		}
		ca, err := hostauth.LoadCA(*caKeyFile, *caCrtFile)
		if err != nil {
			log.Fatalf("Could not load root CA: %v", err)
		}

		csr, priv, err := hostauth.CreateCertificateRequest(pkix.Name{CommonName: flag.Arg(0)}, nil, nil)
		if err != nil {
			log.Fatalf("Could not create CSR: %v", err)
		}

		subcacrt, err := ca.CreateCA(csr)
		if err != nil {
			log.Fatalf("Could not sign CA certificate: %v", err)
		}
		subca := hostauth.NewCA(priv, subcacrt)
		if err := saveCA(subca, *outKeyFile, *outCrtFile); err != nil {
			log.Fatalf("Could not save output: %v", err)
		}

	case *doClient || *doServer:
		hostca, err := hostauth.LoadCA(*caKeyFile, *caCrtFile)
		if err != nil {
			log.Fatalf("Could not load host CA: %v", err)
		}

		var name string
		if flag.NArg() > 0 {
			name = flag.Arg(0)
		}
		csr, priv, err := hostauth.CreateCertificateRequest(hostauth.NewCredentials().Name(name), nil, nil)
		if err != nil {
			log.Fatalf("Could not create CSR: %v", err)
		}

		var crt *x509.Certificate
		if *doClient {
			crt, err = hostca.CreateClientCert(csr)
		} else {
			crt, err = hostca.CreateServerCert(csr)
		}
		if err := saveCert(priv, crt, *outKeyFile, *outCrtFile); err != nil {
			log.Fatalf("Could not save output: %v", err)
		}

	}
}
