package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"syscall"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/appserver"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util/nssutil"
)

var (
	addr              = flag.String("addr", "", "address to listen on")
	numWorkers        = flag.Int("workers", 1, "number of fastcgi workers")
	runDir            = flag.String("run-dir", "", "where to create private fastcgi sockets")
	disableSSL        = flag.Bool("no-ssl", false, "disable hostauth TLS support, serve plain HTTP")
	aclSpec           = flag.String("acl", "*", "ACL for client authorization (default: allow all requests)")
	disableValidation = flag.Bool("no-acl", false, "skip client validation, ignores the value of --acl")
	service           = flag.String("service", "", "service name for hostauth credentials (optional)")
	multiplex         = flag.Bool("multiplex", false, "enable fastcgi request multiplexing (warning: experimental)")

	profile = flag.Bool("profile", false, "enable profiling")
)

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>] <COMMAND> [<ARGS>...]

Serve a FastCGI application over HTTP/HTTPS.

This tool will start a number of FastCGI runners, using UNIX sockets
for communication. The specified COMMAND will be used to start the
runners: the literal sequence '%%s' in COMMAND will be replaced with
the socket path. Arguments to COMMAND will be otherwise passed as-is
to exec().

If hostauth SSL support is enabled, SSL certificates will be
automatically generated and renewed, and the client credentials will
be passed to the FastCGI application via the 'HOSTAUTH_CREDENTIALS'
environment variable.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	if *addr == "" {
		log.Fatal("Must specify --addr. Run `pki-fcgi --help' for details.")
	}
	if flag.NArg() < 1 {
		log.Fatal("Must specify command for FastCGI runner. Run `pki-fcgi --help' for details.")
	}

	if *disableSSL {
		*disableValidation = true
	}

	runtime.GOMAXPROCS(runtime.NumCPU())
	syscall.Setpgid(0, 0)

	var err error
	opts := &appserver.AppServerOptions{
		RunDir:             *runDir,
		NumWorkers:         *numWorkers,
		EnableMultiplexing: *multiplex,
	}
	if !*disableValidation {
		opts.ACL, err = hostauth.ParseACL(*aclSpec)
		if err != nil {
			log.Fatal(err)
		}
		opts.ACLContext = nssutil.NewNSSAclContext()
	}

	apps := appserver.NewFastCGIAppServer(flag.Args(), opts)
	srv := &http.Server{
		Handler: apps,
	}

	sigch := make(chan os.Signal, 1)
	go func() {
		<-sigch
		log.Printf("shutting down")
		apps.Close()
		os.Exit(0)
	}()
	signal.Notify(sigch, syscall.SIGINT, syscall.SIGTERM)

	// With --profile enabled, only run for 60 seconds and then
	// dump CPU/memory profiles.
	if *profile {
		f, _ := os.Create("pki-fcgi.prof")
		pprof.StartCPUProfile(f)
		go func() {
			time.Sleep(60 * time.Second)
			pprof.StopCPUProfile()

			m, _ := os.Create("pki-fcgi.mprof")
			pprof.WriteHeapProfile(m)
			m.Close()

			os.Exit(0)
		}()
	}

	var l net.Listener
	if !*disableSSL {
		c, err := client.New()
		if err != nil {
			log.Fatal(err)
		}
		if *service != "" {
			c.Creds.Service = *service
		}
		defer c.Close()

		l, err = client.Listen(*addr, c)
	} else {
		l, err = net.Listen("tcp", *addr)
	}
	if err != nil {
		log.Fatal(err)
	}

	if err := srv.Serve(l); err != nil {
		log.Fatal(err)
	}
}
