package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util/nssutil"
)

var (
	addr              = flag.String("addr", "", "address to listen on")
	dir               = flag.String("dir", ".", "directory to serve (default: current directory)")
	aclSpec           = flag.String("acl", "*", "ACL for client authorization (default: allow all requests)")
	disableValidation = flag.Bool("no-acl", false, "skip client validation, ignores the value of --acl")
)

func Usage() {
	prog := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage: %s [<OPTIONS>]

Serve static content from a directory over HTTP.

It will automatically renew the server certificate, and it allows
you to set an ACL to control access.

Options:

`, prog)
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
}

type staticServer struct {
	acl    hostauth.ACL
	aclCtx hostauth.ACLContext
	h      http.Handler
}

func (s *staticServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Check the remote peer credentials against our ACL.
	_, err := client.CheckPeerCredentials(r.TLS, s.acl, s.aclCtx)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	s.h.ServeHTTP(w, r)
}

func newStaticServer(h http.Handler) *staticServer {
	acl, err := hostauth.ParseACL(*aclSpec)
	if err != nil {
		log.Fatal(err)
	}
	aclCtx := nssutil.NewNSSAclContext()
	return &staticServer{
		acl:    acl,
		aclCtx: aclCtx,
		h:      h,
	}
}

func main() {
	flag.Usage = Usage
	flag.Parse()
	log.SetFlags(0)

	http.Handle("/", newStaticServer(http.FileServer(http.Dir(*dir))))

	c, err := client.New()
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	l, err := client.Listen(*addr, c)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("starting https server on %s", *addr)
	if err := http.Serve(l, nil); err != nil {
		log.Fatal(err)
	}
}
