package client

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
)

// CheckPeerCredentials validates and returns credential information
// obtained from the certificates sent by the client with the TLS
// connection. It verifies that the validation trust path actually
// goes through the host-specific intermediate key as specified by the
// client credentials.
func CheckPeerCredentials(state *tls.ConnectionState, acl hostauth.ACL, aclCtx hostauth.ACLContext) (hostauth.Credentials, error) {
	var creds hostauth.Credentials

	// Extract credentials from the peer certificate.
	if len(state.PeerCertificates) < 1 {
		return creds, errors.New("missing client certificate")
	}
	crt := state.PeerCertificates[0]
	var err error
	creds, err = hostauth.NewCredentialsFromName(crt.Subject)
	if err != nil {
		return creds, err
	}

	// Validate the trust chain by checking that the intermediate
	// certificate corresponds to the hostname specified in the
	// credentials (so that we can trust the Host field).
	//
	// The expected trust chain is USER_CERT -> HOST_CERT -> ROOT.
	if len(state.VerifiedChains) < 1 {
		return creds, errors.New("no verified trust chain")
	}
	vchain := state.VerifiedChains[0]
	if len(vchain) != 3 {
		return creds, fmt.Errorf("verification chain has unexpected length: %s", hostauth.CertificateChainToString(vchain))
	}
	if vchain[1].Subject.CommonName != creds.Host {
		return creds, fmt.Errorf("bad host certificate in verification chain: %s", hostauth.CertificateChainToString(vchain))
	}

	if acl != nil {
		if !acl.Match(creds, aclCtx) {
			return creds, errors.New("forbidden by ACL")
		}
	}

	return creds, nil
}

// ClientTlsConfig returns a tls.Config suitable for client
// connections to the specified peer. The given peer address will be
// validated against the CN of the remote server's certificate.
//
// The connection handshake will include the local host intermediate
// certificate.
func (c *Client) ClientTlsConfig(addr string) (*tls.Config, error) {
	clientCert, clientKey, err := c.ClientCertificate()
	if err != nil {
		return nil, err
	}

	return RawClientTlsConfig(addr, clientKey, clientCert, c.hostCert, c.Roots), nil
}

func RawClientTlsConfig(addr string, clientKey interface{}, clientCert, hostCert *x509.Certificate, roots *x509.CertPool) *tls.Config {
	addr = stripPort(addr)
	return &tls.Config{
		Certificates: []tls.Certificate{{
			Certificate: [][]byte{clientCert.Raw, hostCert.Raw},
			PrivateKey:  clientKey,
			Leaf:        clientCert,
		}},
		RootCAs:      roots,
		ServerName:   addr,
		MinVersion:   tls.VersionTLS12,
		CipherSuites: hostauth.CipherSuites,
	}
}

// ServerTlsConfig returns a tls.Config suitable for server
// connections listening on the specified address. The address will be
// used as the CN of the generated certificate, along with all the
// known host names and IP addresses of the local host (using X509
// extensions). Clients will need to use one of these known names when
// connecting.
//
// The connection handshake will include the local host intermediate
// certificate.
func (c *Client) ServerTlsConfig(addr string) (*tls.Config, <-chan string, error) {
	serverCert, serverKey, update, err := c.ServerCertificate(addr)
	if err != nil {
		return nil, nil, err
	}

	return RawServerTlsConfig(serverKey, serverCert, c.hostCert, c.Roots), update, nil
}

func RawServerTlsConfig(serverKey interface{}, serverCert, hostCert *x509.Certificate, roots *x509.CertPool) *tls.Config {
	return &tls.Config{
		Certificates: []tls.Certificate{{
			Certificate: [][]byte{serverCert.Raw, hostCert.Raw},
			PrivateKey:  serverKey,
			Leaf:        serverCert,
		}},
		MinVersion:               tls.VersionTLS10,
		CipherSuites:             hostauth.CipherSuites,
		PreferServerCipherSuites: true,
		ClientAuth:               tls.RequireAndVerifyClientCert,
		ClientCAs:                roots,
	}
}

type listener struct {
	*net.TCPListener
	client *Client

	update    <-chan string
	tlsConfig *tls.Config

	KeepAlivePeriod time.Duration
}

var DefaultKeepAlivePeriod = 3 * time.Minute

// Listen creates a TLS TCP listener accepting connection on the
// specified address, which will always use the most up-to-date TLS
// certificates from the given Client.
func Listen(addr string, client *Client) (net.Listener, error) {
	tcpl, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	tlsConfig, update, err := client.ServerTlsConfig(addr)
	if err != nil {
		return nil, err
	}

	l := &listener{
		TCPListener:     tcpl.(*net.TCPListener),
		client:          client,
		update:          update,
		tlsConfig:       tlsConfig,
		KeepAlivePeriod: DefaultKeepAlivePeriod,
	}
	go func() {
		// The hostauth Client says that we can keep using the
		// same update channel returned by ServerTlsConfig the
		// first time, all updates will be sent there.
		//
		// TODO: error handling is clearly insufficient.
		for range update {
			if tlsConfig, _, err := client.ServerTlsConfig(addr); err == nil {
				l.tlsConfig = tlsConfig
			}
		}
	}()
	return l, nil
}

func (l *listener) Accept() (net.Conn, error) {
	c, err := l.TCPListener.AcceptTCP()
	if err != nil {
		return nil, err
	}

	if l.KeepAlivePeriod > 0 {
		c.SetKeepAlive(true)
		c.SetKeepAlivePeriod(l.KeepAlivePeriod)
	}

	if l.tlsConfig.NextProtos == nil {
		l.tlsConfig.NextProtos = []string{"http/1.1"}
	}

	return tls.Server(c, l.tlsConfig), nil
}
