package client

import (
	"crypto/ecdsa"
	"crypto/x509"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"net"
	"sync"
	"time"

	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/local"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/git.autistici.org/ale/hostauth/util"
	"git.autistici.org/ale/sshca/Godeps/_workspace/src/github.com/golang/groupcache/singleflight"
)

var (
	rootCaPath   = flag.String("authd-ca", "/etc/hostauth/root.pem", "location of the root CA public key (PEM format)")
	caSocketPath = flag.String("authd-hostd-socket", "/var/run/hostauth/local", "UNIX socket to listen on")
)

const (
	// Tags used to track expiration of some special certificates.
	hostCertName = "@localhost"
	clientName   = "@client"
)

type keyPair struct {
	Certificate *x509.Certificate
	PrivateKey  *ecdsa.PrivateKey
}

type keyPairUpdate struct {
	keyPair
	Update chan string
}

func LoadRootPool() (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(*rootCaPath)
	if err != nil {
		return nil, err
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(data) {
		return nil, errors.New("no root certificates could be loaded")
	}
	return pool, nil
}

// A Client provides access to signed credentials for an application,
// and takes care of renewing them before they expire.
type Client struct {
	Creds hostauth.Credentials

	Roots  *x509.CertPool
	signer *local.Signer

	hostCertGetter local.HostCertGetter
	hostCert       *x509.Certificate

	lock sync.Mutex

	// Use a single expiration tracker for all our resources.
	expiry *util.Scheduler

	clientCert  *keyPair
	authTicket  string
	serverCerts map[string]*keyPairUpdate
	update      singleflight.Group
}

func New() (*Client, error) {
	return NewClient(nil, nil, nil)
}

func NewClient(signerProto local.SignerProtocol, hostCertGetter local.HostCertGetter, roots *x509.CertPool) (*Client, error) {
	if roots == nil {
		// Load the root CA.
		var err error
		roots, err = LoadRootPool()
		if err != nil {
			return nil, err
		}
	}

	if signerProto == nil {
		signerProto = local.NewSocketSignerProtocol(*caSocketPath)
	}

	if hostCertGetter == nil {
		hostCertGetter = local.DefaultHostCertGetter(roots)
	}

	c := &Client{
		Creds:          hostauth.NewCredentials(),
		Roots:          roots,
		signer:         local.NewSigner(signerProto),
		hostCertGetter: hostCertGetter,
		expiry:         util.NewScheduler(),
		serverCerts:    make(map[string]*keyPairUpdate),
	}

	c.updateHostCert()
	go c.refresher()
	return c, nil
}

func (c *Client) Close() error {
	c.expiry.Close()
	return nil
}

func (c *Client) updateHostCert() {
	for {
		hostCert, err := c.hostCertGetter.GetHostCertificate()
		if err == nil {
			deadline := hostauth.ExpirationDeadline(hostCert)
			if deadline.Before(time.Now()) {
				// Oops, something went wrong and the
				// host certificate we retrieved has
				// expired. This can mean that the
				// host has been disconnected from the
				// network for a while -- in any case,
				// manual intervention is required.
				//
				// There isn't anything we can do
				// besides waiting a bit and retrying.
				log.Printf("host certificate is expired! retrying in 10 seconds")
				deadline = time.Now().Add(10 * time.Second)
			}
			c.hostCert = hostCert
			c.expiry.Add(hostCertName, deadline)
			log.Printf("fetched host certificate: %s", hostauth.CertificateToString(hostCert))

			// When the host certificate is updated, tell
			// all the server listeners to refresh their
			// credentials.
			c.lock.Lock()
			for name, kpu := range c.serverCerts {
				select {
				case kpu.Update <- name:
				default:
				}
			}
			c.lock.Unlock()

			return
		} else {
			log.Printf("error fetching host certificate: %v", err)
		}
		time.Sleep(30 * time.Second)
	}
}

func (c *Client) updateClientCert() {
	// Retry forever.
	for {
		crt, key, tkt, err := c.signer.Sign(c.Creds, false, nil, nil)
		if err == nil {
			log.Printf("got client credentials: %s", hostauth.CertificateToString(crt))
			c.clientCert = &keyPair{Certificate: crt, PrivateKey: key}
			c.authTicket = tkt
			c.expiry.Add(clientName, hostauth.ExpirationDeadline(crt))
			return
		} else {
			log.Printf("error acquiring client credentials: %v", err)
		}
		time.Sleep(30 * time.Second)
	}
}

func addToLocalHostnames(name string) []string {
	out := make([]string, 0, len(util.Hostnames)+1)
	out = append(out, name)
	for _, n := range util.Hostnames {
		if n != name {
			out = append(out, n)
		}
	}
	return out
}

func (c *Client) updateServerCert(name string) (*keyPairUpdate, error) {
	log.Printf("server certificate for %s is about to expire", name)
	// Retry forever.
	for {
		// When building a server certificate, the specified
		// name is added to the list of known dns names and ip
		// addresses of the local host, to maximize the
		// likelihood of success of CN verification for remote
		// clients.
		crt, key, _, err := c.signer.Sign(c.Creds, true, addToLocalHostnames(name), util.LocalIPs)
		if err == nil {

			// Update the certificate, but keep the
			// channel if it already exists.
			c.lock.Lock()
			kpu, ok := c.serverCerts[name]
			if !ok {
				kpu = &keyPairUpdate{
					Update: make(chan string, 1),
				}
				c.serverCerts[name] = kpu
			}
			kpu.PrivateKey = key
			kpu.Certificate = crt

			c.expiry.Add(name, hostauth.ExpirationDeadline(crt))

			select {
			case kpu.Update <- name:
			default:
			}

			c.lock.Unlock()
			log.Printf("got server credentials: %s", hostauth.CertificateToString(crt))
			return kpu, nil
		} else {
			log.Printf("error acquiring server credentials for %s: %v", name, err)
		}
		time.Sleep(30 * time.Second)
	}
}

func (c *Client) refresher() {
	for name := range c.expiry.C {
		switch name {
		case clientName:
			go c.updateClientCert()
		case hostCertName:
			go c.updateHostCert()
		default:
			go c.updateServerCert(name)
		}
	}
}

func (c *Client) HostCertificate() *x509.Certificate {
	return c.hostCert
}

func (c *Client) loadClientCert() {
	if c.clientCert == nil {
		c.update.Do(clientName, func() (interface{}, error) {
			c.updateClientCert()
			return nil, nil
		})
	}
}

func (c *Client) ClientCertificate() (*x509.Certificate, interface{}, error) {
	c.loadClientCert()
	return c.clientCert.Certificate, c.clientCert.PrivateKey, nil
}

func (c *Client) AuthTicket() (string, error) {
	c.loadClientCert()
	return c.authTicket, nil
}

func (c *Client) ServerCertificate(addr string) (*x509.Certificate, interface{}, <-chan string, error) {
	addr = stripPort(addr)
	if addr == "" {
		addr = c.Creds.Host
	}

	c.lock.Lock()
	kpu, ok := c.serverCerts[addr]
	c.lock.Unlock()
	if !ok {
		result, _ := c.update.Do(addr, func() (interface{}, error) {
			return c.updateServerCert(addr)
		})
		kpu = result.(*keyPairUpdate)
	}

	return kpu.Certificate, kpu.PrivateKey, kpu.Update, nil
}

func stripPort(addr string) string {
	if host, _, err := net.SplitHostPort(addr); err == nil {
		return host
	}
	return addr
}
